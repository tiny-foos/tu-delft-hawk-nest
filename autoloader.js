if (location.protocol === 'https:'){
  location.protocol = "http:";
}
console.log('Sorry for using non secure version, images are available only over the HTTP and Chrome does not allow mixed content by default');

document.addEventListener("DOMContentLoaded", function () {
  var location = document.getElementById('location').value;

  var loadingCheckInterval;
  var switchInterval;
  var imageIndex = 60;
  var loadedImagesCount = 0;
  var frame = document.getElementById('frame');
  var loadingLabel = document.getElementById('loading');
  var switchIntervalTime = 1000;

  var INSIDE_IMAGES_URL = 'http://slechtvalk.bk.tudelft.nl/stills/last';
  var OUTSIDE_IMAGES_URL = 'http://slechtvalk.bk.tudelft.nl/stills/lest';

  var BASE_URL = location === 'inside' ? INSIDE_IMAGES_URL : OUTSIDE_IMAGES_URL;

  function updateSwitchInterval() {
    document.getElementById('interval').innerHTML = switchIntervalTime + ' ms';
    clearInterval(switchInterval);
    switchInterval = setInterval(switchIntervalHandler, switchIntervalTime);
  }

  document.getElementById('slower').addEventListener("click", function () {
    if (switchIntervalTime > 0) {
        switchIntervalTime -= 100;
        updateSwitchInterval();
    }
  });

  document.getElementById('faster').addEventListener("click", function () {
    if (switchIntervalTime < 10000) {
        switchIntervalTime += 100;
        updateSwitchInterval();
    }
  });

  var checkLoadedInterval = function() {
    if (loadedImagesCount >= 60){
      clearInterval(loadingCheckInterval);
      loadingLabel.className = 'hidden';
      document.getElementById('time_control').className='';
      switchInterval = setInterval(switchIntervalHandler, switchIntervalTime);
      setInterval(newImageIntervalHandler, 60000);
    }else{
      loadingLabel.innerHTML = 'Loading images... ' + loadedImagesCount + ' / 60';
    }
  }

  loadingCheckInterval = setInterval(checkLoadedInterval, 1000);

  for (var i = 60; i >= 1; i--){
    var newImage = document.createElement('img');
    newImage.id = 'image' + i;
    newImage.src = BASE_URL + i + '.jpg';
    newImage.className = 'hidden';
    newImage.onload = function(){
      loadedImagesCount++;
    }

    frame.appendChild(newImage);
  }

  document.getElementById('image60').className = 'visible';



  var switchIntervalHandler = function() {
    try{
      document.getElementById('image' + imageIndex).className = 'hidden';
    }catch (e) {
      console.error(imageIndex);
      console.error(e);
    }
    if (--imageIndex === 0){
      imageIndex = 60;
    }
    document.getElementById('image' + imageIndex).className = 'visible';
  }

  var newImageIntervalHandler = function () {
    clearInterval(switchInterval);

    var lastImage = document.getElementById('image60');
    frame.removeChild(lastImage);

    for (var i = 59; i >= 1; i--){
      var image = document.getElementById('image' + i);
      image.id = 'image' + (i+1);
    }

    if (++imageIndex == 61) {
      imageIndex = 60;
    }

    var newImage = document.createElement('img');
    newImage.id = 'image1';
    newImage.src = BASE_URL + '1.jpg';
    newImage.className = 'hidden';

    frame.appendChild(newImage);

    switchInterval = setInterval(switchIntervalHandler, switchIntervalTime);
  }
});